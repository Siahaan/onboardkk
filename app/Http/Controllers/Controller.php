<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use App\{Branch, Outlet, Merchant};

class Controller extends BaseController {

    use AuthorizesRequests,
        DispatchesJobs,
        ValidatesRequests;

    public function index(Request $request) {
        Session::flush();
        $code = $request->code ? $request->code : $request->input('\code'); // ini biarin tetep gini bgst mo lokal mo prod juga biarin begini
        //dd ($request->all());
	
        if ($code) {
            # Setup request to send json via POST.
            $ch = curl_init();
            $headers = [
                'Content-Type: text/plain'
            ];
            $postData = [
                'code' => $code,
            ];
	    //dd($code);

            curl_setopt($ch, CURLOPT_URL, "http://35.240.167.11:10001/v1/kitakitchen/merchant/information/");
            //curl_setopt($ch, CURLOPT_POST, 1);
	    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            //acurl_setopt($ch, CURLOPT_POSTFIELDS,"{\n    \"code\":$code\n}");
	    curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($postData));
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            $result = curl_exec($ch); // ini data sebenarnya
            $statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

            $json = json_decode($result);

            if (isset($json->error)) {
                return view('error');
            }
            else
            {
              // $result = '{"mid_pos":"213224","name":"Moka Cool ShoP","address":"Kencana Tower, Business Park Kb. Jeruk, Jl. Meruya Ilir Raya No.108","city":"Kota Jakarta Barat","province":"Kota Jakarta Barat","postal_code":"11620","email":"integration@mokapos.com","phone_number":"0857146834500","website":"www.mokapos.com","social_media":{"twitter":"","facebook":"","instagram":"mokapos"}}';
              $json = json_decode($result);
              $name = $json->name;
              $mid_pos = $json->mid_pos;
              Session::put('name', $name);
              Session::put('mid_pos', $mid_pos);
              $data['result'] = $json;
              return response()->view('welcome', $data);
            }
          } else {
              return view('error');
        }
    }

    public function toOutlet(Request $request) {
        $midpos = $request->mid_pos ? $request->mid_pos :$request->input('\mid_pos');
     
        $ch = curl_init();
        //echo $ch;
	$headers = ['Content-Type: text/plain'];
        $postData = ['business_id' => (int)$midpos];
        curl_setopt($ch, CURLOPT_URL, "http://35.240.167.11:10001/kitakitchen/v1/outlets/");
	//curl_setopt($ch, CURLOPT_URL, "http://127.0.0.1:10001/kitakitchen/v1/outlets/");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($postData));
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        
	$result = curl_exec($ch); // ini data sebenarnya

	// dd($result);
        $statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
//        $result = '{"data":{"outlets":[{"address":"Ruko Golf Island Blok L No.19","business_id":227562,"city":"Kota Jakarta Utara","id":260177,"is_deleted":false,"name":"Kopi Konnichiwa","notes":"Wifi : Konnichiwa\nPassword : arigatou","phone_number":"081280270697","postal_code":"14470","province":"DKI Jakarta"},'
//                . '{"address":"jl. test alamat1","business_id":227562,"city":"Kota Jakarta Selatan","id":260178,"is_deleted":false,"name":"Kopi Kenangam","notes":"Wifi : Konnichiwa\nPassword : arigatou","phone_number":"081280270697","postal_code":"14471","province":"DKI Jakarta"},'
//                . '{"address":"jl. test alamat1","business_id":227562,"city":"Kota Jakarta Barat","id":260179,"is_deleted":false,"name":"Kopi Starbuck","notes":"Wifi : Konnichiwa\nPassword : arigatou","phone_number":"081280270697","postal_code":"14472","province":"DKI Jakarta"}],'
//                . '"total_count":1},"meta":{"code":200,"errors":{}}}';

        $body = json_decode($result);
	//dd($body);
        if ($body) {
            DB::table('temp_outlet_moka')->truncate();
            $out = $body->data->outlets;
            foreach ($out as $o) {
                DB::table('temp_outlet_moka')
                        ->insert([
                            'address' => $o->address,
                            'business_id' => $o->business_id,
                            'city' => $o->city,
                            'outlet_code' => $o->id,
                            'business_id' => $o->business_id,
                            'is_deleted' => $o->is_deleted,
                            'name' => $o->name,
                            'phone_number' => $o->phone_number,
                            'province' => $o->province,
                            'postal_code' => $o->postal_code,
                ]);
            }

            $data['outlets'] = DB::table('temp_outlet_moka')->get();
	    $outletSession = Session::get('outlet');
            if ($outletSession) {
              $pluckOutletId = array_column($outletSession, 'outlet_code');
            }else{
              $pluckOutletId = [];
            }

            $data['outlet_code'] = $pluckOutletId;
            // if (Session::get('outlet')) {
            //     $s_ot = Session::get('outlet');
            //     $s_id = [];
            //     foreach ($s_ot as $s_t) {
            //         $id = $s_t->outlet_code;
            //         array_push($s_id, $id);
            //     }
            //     $data['s_outlet'] = DB::table('temp_outlet_moka')->whereIn('outlet_code', $s_id)->get();
            //     $data['un_outlet'] = DB::table('temp_outlet_moka')->whereNotIn('outlet_code', $s_id)->get();
            // }
            return view('daftar_outlet', $data);
        } else {
            abort(404);
        }
    }

    public function doOutlet(Request $request) {
        Session::forget('outlet');
	$midpos = $request->mid_pos ? $request->mid_pos :$request->input('\mid_pos');
        $data_arr = [];
        if ($request->outlet) {
            foreach ($request->outlet as $key => $value) {
                $data = json_decode($value);
                array_push($data_arr, $data);
            }
        }
        Session::put('outlet', $data_arr);
        //return redirect('konfirmasi?mid_pos=' . $request->mid_pos);
	return redirect('konfirmasi?mid_pos=' . $midpos);
    }

    public function doDelete(Request $request)
    {
        $index = $request->index;
        $newOutlet = [];
        $newBranch = [];
        foreach (Session::get('outlet') as $key => $value) {
          if ($key != $index) {
            array_push($newOutlet, $value);
          }
        }

        foreach (Session::get('branch') as $key => $value) {
          if ($key != $index) {
            array_push($newBranch, $value);
          }
        }

        Session::flush('outlet');
        Session::flush('branch');

        Session::put('outlet', $newOutlet);
        Session::put('branch', $newBranch);

        return redirect()->back();
    }

    public function toKonformasi(Request $request) {
// Session::forget('outlet');
// Session::forget('branches');
        //$data['mpost'] = $request->mid_pos;
	$data['mpost'] = $request->mid_pos ? $request->mid_pos : $request->input('\mid_pos');
	//dd($data);
	$outlet = $request->session()->get('outlet');
        $branch = $request->session()->get('branch');

        $data['outlet'] = $outlet;
        $data['branches'] = $branch;

        $c_outlet = $outlet ? count($outlet) : 0;
        $c_branch = $branch ? count($branch) : 0;

        $data['count'] = $c_outlet > $c_branch ? $c_outlet : $c_branch;
        return view('konfirmasi', $data);
    }

    public function doBranch(Request $request) {
        Session::forget('branch');
	$midpos = $request->mid_pos ? $request->mid_pos :$request->input('\mid_pos');
        $data_arr = [];
        if ($request->branch) {
            foreach ($request->branch as $key => $value) {
                $data = json_decode($value);
                array_push($data_arr, $data);
            }
        }
        Session::put('branch', $data_arr);

        //return redirect('konfirmasi?mid_pos=' . $request->mid_pos);
	return redirect('konfirmasi?mid_pos=' . $midpos);
    }

    public function toBranch(Request $request) {
//        Session::forget('branches');
        $data['branches'] = Branch::where('active', 1)->get();
        if (Session::get('branch')) {
                $s_ot = Session::get('branch');
                $s_id = [];
                foreach ($s_ot as $s_t) {
                    $id = $s_t->id;
                    array_push($s_id, $id);
                }
                $data['s_branch'] = DB::table('branches')->whereIn('id', $s_id)->get();
                $data['un_branch'] = DB::table('branches')->whereNotIn('id', $s_id)->get();
            }
//            dd($data);

        return view('daftar_branch', $data);
    }

    public function doApprove(request $request) {
        $outlet = $request->session()->get('outlet');
        $branch = $request->session()->get('branch');
	$merchant = Merchant::where('mid_pos', $request->mid_pos)->first();

        foreach ($outlet as $key => $value) {
            // $mer_id = DB::table('merchants')->select('id')->where('mid',Session::get('mid_pos'))->first();
            // $id=$mer_id->id;
            $save = new Outlet;
            $save->outlet_code = $outlet[$key]->outlet_code;
            $save->merchant_id = $merchant->id;
            $save->status = 1;
            $save->name = $outlet[$key]->name;
            $save->phone_no = $outlet[$key]->phone_number;
            $save->provider_code = "MOKA";
            $save->branches_id = $branch[$key]->id;
            $save->save();
        }

        return redirect('outlet/done');
    }

    public function toDone() {
        return view('done');
    }

}

