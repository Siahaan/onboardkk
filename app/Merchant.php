<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Merchant extends Authenticatable {

    use Notifiable;

    protected $table = 'merchants';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'merchant_name', 'active', 'mid', 'token', 'refresh_token', 'bussiness_id', 'address', 'city', 'province', 'postal_code', 'email', 'phone_number', 'website', 'instagram', 'twitter', 'latitude', 'longitude', 'created_at', 'created_by', 'updated_at', 'updated_by', 'pos', 'mid_pos', 'company_name', 'category', 'sub_category'
    ];

}
