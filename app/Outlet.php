<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Outlet extends Model
{
    protected $table = 'outlet';

    protected $fillable = [
        'outlet_code', 'sid', 'merchant_id', 'provider_code', 'status', 'name', 'phone_no', 'branches_id',
    ];
    
    public $timestamps = false;

}