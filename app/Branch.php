<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    protected $table = 'branches';

    protected $fillable = [
        'active','name','address','city','province','postal_code','phone_number','latitude','longitude','created_by','created_at','updated_by','updated_at','product_type',
    ];
    
    protected $casts = [
    ];
}