
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>KitaKitchen</title>

    <!-- Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Rubik' rel='stylesheet'>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ asset('/css/newcss.css') }}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

</head>
<body class="ss6 fontfamsi" style="background-color: black; max-height: 100%; padding: 100px">
    <div class="container-xl">
        <div class="content" >
            
            <div class="row justify-content-md-center" style="margin-top: 22px">
                <b style="font-size: 48px;color: white;">:(</b>
            </div>
            <div class="row justify-content-md-center">
                <b style="font-size: 16px;color: white;text-align: center;">Failed : Key Expired</b>
            </div>
            
        </div>
    </div>



</body>

</html>
