<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>KitaKitchen</title>

        <!-- Fonts -->
        <link href='https://fonts.googleapis.com/css?family=Rubik' rel='stylesheet'>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="{{ asset('/css/newcss.css') }}">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    </head>
    <style>
        .check-with-label:checked + .label-for-check {
            background: #ff3333;
            color: #fffaf5;
            border-color: #ff3333;
        }
    </style>
    <body class="ss6 fontfamsi">
        <form method="post">
            {{csrf_field()}}
            <div class="container-xl">
                <div class="content">
                    <div class="row justify-content-md-center" >
                        <div class="ss1 col-md-3">
                            <img class="ss4" src="{{asset('/images/kita-kitchen-logo-standard-red@3x.png')}}">
                        </div>
                    </div>
                    <div  class="row justify-content-md-center" >
                        <div class="col-md-6 ss9 tcntr" >
                            <b>Choose {{session('name')}} Outlet location you wish to share Merchant Profile and Data Transaction to Kita Kitchen</b>
                        </div>
                    </div>
                    <div class="row justify-content-md-center">
                        <div class="col-md-3 ss10 tcntr" >
                            <b>*You can choose more than 1 outlet</b>
                        </div>
                    </div>

                    <div class="row justify-content-md-center"  style="margin-top:2%">
                        <div class="input-group col-md-4">
                            <input class="form-control py-2 border-right-0 border src_outlet" type="search" placeholder="Search Outlet Location"
                                   id="oulet-search" onkeyup="src_outlet()">
                            <span class="input-group-append">
                                <div class="input-group-text bg-transparent border-left-0" style="border: solid 1px #dcdcdc;border-radius: 0 16px 16px 0;"><i class="fa fa-search" style="font-size: 12px;"></i></div>
                            </span>
                        </div>
                    </div>
                    <div class="row justify-content-md-center" style="margin-top:2.2%">
                        <div class="col-md-10 " style="background: #fffaf5;border-radius: 25px;box-shadow: 0 5px 10px 0 rgba(0, 0, 0, 0.08); padding-top: 35px;padding-bottom: 35px;">
                            <div class="ex3 col-12">
                                <div class="row rwbtn" id="dip">


                                    @foreach($outlets as $row)

                                    <div class="col-md-3" style="margin-bottom:15px;padding-left: 0;padding-right: 15px">
                                        <input id="{{$row->id}}" type="checkbox" {{ in_array($row->outlet_code, $outlet_code) ? "checked='true'" : '' }} class="form-check list-group-item check-with-label" style="display:none" name="outlet[]" value="{{json_encode($row)}}" >
                                        <label for="{{$row->id}}" class="btn btn-danger mselbtn  label-for-check" style="padding-bottom: 15px;margin-right: 15px">
                                            <b style="font-size:18px;" id="name_ot">{{$row->name}}</b>
                                            <p style="font-size:8px;font-weight: bold;margin-bottom: 0;margin-top: 5px">{{$row->address}}<br> {{$row->city}}, {{$row->province}} {{$row->postal_code}}</p>
                                        </label>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-md-center" style="margin-top:4%;margin-bottom: 10%">
                        <div class="col-md-2">
                            <button type="submit" name="button" class="btn btn-danger btnchs">Choose</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </body>
    <script>
        function src_outlet() {
            // Declare variables
            var input, filter, table, tr, td, i, txtValue;
            input = document.getElementById("oulet-search");
            filter = input.value.toUpperCase();
            table = document.getElementById("dip");
            tr = table.getElementsByTagName("div");

            for (i = 0; i < tr.length; i++) {
                td = tr[i].getElementsByTagName("b")[0];
                if (td) {
                    txtValue = td.textContent || td.innerText;
                    if (txtValue.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "block";
                    } else {
                        tr[i].style.display = "none";
                    }
                }
            }
        }
    </script>
</html>

