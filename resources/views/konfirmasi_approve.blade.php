<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>KitaKitchen</title>

    <!-- Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Rubik' rel='stylesheet'>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

</head>
<style>
    .container1{
        width: 100%;
        position: absolute;
        z-index: 1;
    }
    .progressbar li{
        float: left;
        width: 20%;
        position: relative;
        text-align: center;
        list-style-type: none;
        display: inline-block;
    }
    .progressbar{
        counter-reset: step;
    }
    .progressbar li:before{
        content:counter(step);
        counter-increment: step;
        width: 20px;
        height: 20px;
        border: 3px solid #ff3333;
        display: block;
        margin: 0 auto 10px auto;
        border-radius: 50%;
        line-height: 27px;
        background: #fffaf5;
        color: transparent;
        text-align: center;
        font-weight: bold;
    }
    .progressbar li:after{
        content: '';
        position: absolute;
        width:100%;
        height: 1px;
        background: #979797;
        top: 10px;
        left: -50%;
        z-index: -1;
    }
    .progressbar li:first-child:after{
        content: none;
    }
    .progressbar li.active + li:after{
        background: #dedede;
    }
    .progressbar li.active + li:before{
        border-color: #dedede;
        background: #dedede;
    }
    .bt1{
        background: none;
        border: solid 2px #dcdcdc;
        color: #ff3333;
        font-weight: bold;
        border-radius: 22px;
        font-size: 12px;
        width: 100%;
        height: 51px;
    }
    .bt1:hover{
        background: #ff3333;
        color: white;
        border: solid 2px #ff3333;
    }
</style>
<body style="background: #fffaf5;font-family: Rubik;">
    <div class="container-xl">
        <div class="content">
            <div style="margin: 3.7% 46.2% 6% 46.2%;">
                <img style="width:100%; height: auto;" src="{{asset('/images/kita-kitchen-logo-standard-red@3x.png')}}">
            </div>
            <div>
                <ul class="progressbar" >
                    <li class="active" style="margin-left: 28%;color:#ff3333;font-size: 11px "><b>Share Data to</b></li>
                    <li style="margin-right: 28%;color: #dedede; font-size: 11px;margin-bottom:6.1%"><b>Confirmation</b></li>
                </ul>
            </div>
            <div>
                <table class="table">
                    <thead style="text-align:center;font-size: 18px;color: #000000;font-weight: bold;">
                        <tr>
                            <td style="border-top-color: transparent">Choosed [Merchant.Name] Outlet</td>
                            <td style="border-top-color: transparent"></td>
                            <td style="border-top-color: transparent">Choosed Kita Kitchen Branch</td>
                        </tr>
                    </thead>
                    <tbody style="text-align:center">
                        @if($outlet || $branches)
                        @for($i=0; $i < $count; $i++)
                        <tr>
                            {{dd('disini')}}

                            @if(isset($outlet[$i]))
                            <td style="width:45%;">
                                <!-- <button class="btn btn-danger bt1" style="text-transform: uppercase">Choose [Merchant.Name] Outlet</button> -->
                                <a href="{{ url('outlet').'?mid_pos='.$mpost }}" class="btn btn-danger bt1" style="text-transform: uppercase"> {{ $outlet[$i]->name }} Outlet</a>
                            </td>
                            @else
                            <td style="width:45%;">
                                <!-- <button class="btn btn-danger bt1" style="text-transform: uppercase">Choose [Merchant.Name] Outlet</button> -->
                                <a href="{{ url('outlet').'?mid_pos='.$mpost }}" class="btn btn-danger bt1" style="text-transform: uppercase"> [merchant_name] Outlet</a>
                            </td>
                            @endif
                            <td style="vertical-align:middle;font-size: 10px;font-weight: bold;color: #000000;">Share Data to</td>
                            @if(isset($branches[$i]))
                            <td style="width:45%">
                                <!-- <button class="btn btn-danger bt1" style="text-transform: uppercase">Choose kita kitchen branch</button> -->
                                <a href="{{ url('branch') }}" class="btn btn-danger bt1" style="text-transform: uppercase">{{ $branches[$i]->name }} {{ $branches[$i]->address}} kitchen branch</a>
                            </td>
                            @else
                            <td style="width:45%">
                                <!-- <button class="btn btn-danger bt1" style="text-transform: uppercase">Choose kita kitchen branch</button> -->
                                <a href="{{ url('branch') }}" class="btn btn-danger bt1" style="text-transform: uppercase">Choose kitchen branch</a>
                            </td>
                            @endif
                        </tr>
                        @endfor
                        @else
                        <tr>
                            <td style="width:45%;">
                                <!-- <button class="btn btn-danger bt1" style="text-transform: uppercase">Choose [Merchant.Name] Outlet</button> -->
                                <a href="{{ url('outlet').'?mid_pos='.$mpost }}" class="btn btn-danger bt1" style="text-transform: uppercase"> [merchant_name] Outlet</a>
                            </td>
                            <td style="vertical-align:middle;font-size: 10px;font-weight: bold;color: #000000;">Share Data to</td>
                            <td style="width:45%">
                                <!-- <button class="btn btn-danger bt1" style="text-transform: uppercase">Choose kita kitchen branch</button> -->
                                <a href="{{ url('branch') }}" class="btn btn-danger bt1" style="text-transform: uppercase">Choose kitchen branch</a>
                            </td>
                        </tr>
                        @endif
                    </tbody>
                    <tfoot>
                        <tr>
                            <td style="text-align: center">
                                <a href="{{url('konfirmasi')}}" class="btn btn-danger btnchs">cancel</a>
                                <a href="{{url('process-approve')}}" class="btn btn-danger btnchs">Approve</a>
                            </tr>
                            <td>
                            </td>
                            <td></td>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <div class="row justify-content-md-center" id="ferif" style="display:flex">
                <div class="col-md-2">
                    <button class="btn btn-secondary ss11" onclick="ferif(this)">Next</button>
                </div>
            </div>
            {{ dd('heeeeeeeeeeeh')}}
            <form method="post" action="{{ url('outlet/register')}}">
                {{ csrf_field() }}
                <div class="row justify-content-md-center"  id="ferif1" style="display:none">
                    <div class="col-md-2">
                        <button class="btn btn-danger ss11 btnchs" onclick="back(this)">Cancel</button>
                    </div>
                    <div class="col-md-4" style="margin:45px 0">
                        <table>
                            <tr>
                                <td style="padding-right: 10px"><input type="radio" id="rad" class="rad" onclick="konf1(this)" required></td>
                                <td style="font-size: 14px;font-weight: bold;color: #000000;">By clicking accept, you agree to share Merchant Profile Data and Data Transaction to us</td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-md-2">
                        <button type="submit" class="btn btn-secondary ss11" id="acc">Accept</button>
                        {{-- <button type="submit" class="btn btn-secondary ss11" id="acc" data-toggle="modal" data-target="#finalModal" disabled="true">Accept</button> --}}
                    </div>
                </div>
            </form>

        </div>
    </div>
    {{-- <div class="modal fade" id="finalModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="background: #fffaf5">
        <div class="modal-dialog" role="document" style="max-width: 1000px">
            <div class="modal-content" style="background: #ff3333;border-radius: 50px;width: 100%;height: 567px;border: none">
                <div class="modal-header" style="border-bottom: none">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <!--<span aria-hidden="true">&times;</span>-->
                    </button>
                </div>
                <div class="modal-body" >
                    <div class="row justify-content-md-center">
                        <b style="font-size: 30px;color: white">KK</b>
                    </div>
                    <div class="row justify-content-md-center" style="margin-top: 81px">
                        <i class="fa fa-check-circle" style="font-size:148px;color: #fffaf5;"></i>
                    </div>
                    <div class="row justify-content-md-center" style="margin-top: 22px">
                        <b style="font-size: 48px;color: #fffaf5;">Thank You!</b>
                    </div>
                    <div class="row justify-content-md-center">
                        <b style="font-size: 16px;color: #ffffff;text-align: center;">Onboarding Merchant is being processed.</b>
                    </div>
                    <div class="row justify-content-md-center" style="margin-top: 23px">
                        <b style="font-size: 14px;color: #ffffff;text-align: center;">You will get username & password to access <br>Merchant Dashboard which will be sent by email.</b>
                    </div>
                </div>
                <div class="modal-footer" style="border-top: none">
                </div>
            </div>
        </div>
    </div> --}}
</body>
<script>
    function ferif() {
        var x = document.getElementById("ferif1");
        var y = document.getElementById("ferif");
        var a = document.getElementById("ferif2");
        var b = document.getElementById("ferif3");
        y.style.display = "none";
        x.style.display = "flex";
        a.style.display = "none";
        b.style.display = "flex";
    }
    function konf1() {
        var t = document.getElementById("acc");
        t.disabled = false;
        t.style.background = "#ff3333";
        t.style.borderColor = "#ff3333";
    }
    function back() {
        var y = document.getElementById("ferif");
        var x = document.getElementById("ferif1");
        y.style.display = "flex";
        x.style.display = "none";
    }
</script>
</html>
