
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>KitaKitchen</title>

        <!-- Fonts -->
        <link href='https://fonts.googleapis.com/css?family=Rubik' rel='stylesheet'>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="{{ asset('/css/newcss.css') }}">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    </head>
    <body class="ss6 fontfamsi">
        <div class="container-xl">
            <div class="content">
                <div class="row justify-content-md-center" style="margin-top: 20px">
                    <div class="col-md-11" style="background-color: #ff3333;border-radius: 50px;height: 567px;">
                        <div class="row justify-content-md-center">
                            <b style="font-size: 30px;color: white;margin-top: 20px">KK</b>
                        </div>
                        <div class="row justify-content-md-center" style="margin-top: 81px">
                            <i class="fa fa-check-circle" style="font-size:148px;color: white;"></i>
                        </div>
                        <div class="row justify-content-md-center" style="margin-top: 22px">
                            <b style="font-size: 48px;color: #fffaf5;">Thank You!</b>
                        </div>
                        <div class="row justify-content-md-center">
                            <b style="font-size: 16px;color: #ffffff;text-align: center;">Onboarding Merchant is being processed.</b>
                        </div>
                        <div class="row justify-content-md-center" style="margin-top: 23px">
                            <b style="font-size: 14px;color: #ffffff;text-align: center;">You will get username & password to access <br>Merchant Dashboard which will be sent by email.</b>
                        </div>
                    </div>

                </div>
            </div>
        </div>



    </body>
    <script>
$(document).ready(function () {
    $('#agree').prop('checked', false);
})

function ferif() {
    var x = document.getElementById("ferif1");
    var y = document.getElementById("ferif");
    var a = document.getElementById("ferif2");
    var b = document.getElementById("ferif3");
    y.style.display = "none";
    x.style.display = "flex";
    a.style.display = "none";
    b.style.display = "flex";
}
function konf1() {
    var t = document.getElementById("acc");
    t.disabled = false;
    t.style.background = "#ff3333";
    t.style.borderColor = "#ff3333";
}
function back() {
    var y = document.getElementById("ferif");
    var x = document.getElementById("ferif1");
    y.style.display = "flex";
    x.style.display = "none";
}


$('#agree').on('click', function () {
    var elemen = $('#acc');
    if ($(this).is(":checked")) {
        elemen.prop('disabled', false);
        elemen.css('background-color', '#ff3333');
        elemen.css('border-color', '#ff3333');
    } else {
        elemen.prop('disabled', true);
        elemen.css('background-color', '#6c757d');
        elemen.css('border-color', '#6c757d');
    }
})
    </script>
</html>
