<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>KitaKitchen</title>

        <!-- Fonts -->
        <link href='https://fonts.googleapis.com/css?family=Rubik' rel='stylesheet'>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="{{ asset('/css/newcss.css') }}">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    </head>
    <body class="ss6 fontfamsi">
        <div class="container-xl">
            <div class="content">
                <div class="row justify-content-md-center" >
                    <div class="ss1 col-md-3">
                        <img class="ss4" src="{{asset('/images/kita-kitchen-logo-standard-red@3x.png')}}">
                    </div>
                </div>
                <div class="row justify-content-md-center" id="ferif2" style="display: flex">
                    <div class="col-md-4">
                        <ul class="progressbar1" >
                            <li id="l1" class="active ss7"><b>Share Data to</b></li>
                            <li id="l2" class="ss8"><b>Confirmation</b></li>
                        </ul>
                    </div>
                </div>
                <div class="row justify-content-md-center" id="ferif3" style="display: none">
                    <div class="col-md-4">
                        <ul class="progressbar2" >
                            <li id="l3" class="ss12"><b>Share Data to</b></li>
                            <li id="l4" class="active ss7"><b>Confirmation</b></li>
                        </ul>
                    </div>
                </div>
                <div>
                    <table class="table" style="margin-top:5%">
                        <thead class="tbhead1" >
                            <tr id="ontbl">
                                <td >Choosed {{session('name')}} Outlet</td>
                                <td ></td>
                                <td >Choosed Kita Kitchen Branch</td>
                                <td></td>
                            </tr>
                        </thead>
                        <tbody class="tcntr">
                            @if($outlet || $branches)
                            @for($i=0; $i < $count; $i++)
                            @php
                              $ocode = null;
                              $bcode = null;
                            @endphp
                            <tr>
                                @if(isset($outlet[$i]))
                                <td style="width:45%;">
                                    <a href="{{ url('outlet').'?mid_pos='.$mpost }}" class="btn btn-danger bt1-1">
                                        <div class="row" style="padding: 0 .75rem 0 1.25rem;">
                                            <div class="col-md-5" style="padding-top: 7px;text-transform: uppercase">
                                                {{ $outlet[$i]->name }}
                                            </div>
                                            <div class="col-md-7" style="font-size: 9px;">
                                                {{ $outlet[$i]->address }}<br>{{ $outlet[$i]->city }}, {{ $outlet[$i]->province }} {{ $outlet[$i]->postal_code }}
                                            </div>
                                        </div>
                                    </a>
                                </td>
                                @php $ocode = $outlet[$i]->outlet_code  @endphp
                                @else
                                <td style="width:45%;">
                                    <a href="{{ url('outlet').'?mid_pos='.$mpost }}" class="btn btn-danger bt1" >Choose {{session('name')}} Outlet</a>
                                </td>
                                @endif
                                <td style="vertical-align:middle;font-size: 10px;font-weight: bold;color: #000000;">Share Data to</td>
                                @if(isset($branches[$i]))
                                <td style="width:45%">
                                    <a href="{{ url('branch').'?mid_pos='.$mpost }}" class="btn btn-danger bt1-1">
                                        <div class="row" style="padding: 0 .75rem 0 1.25rem;">
                                            <div class="col-md-5" style="padding-top: 7px;text-transform: uppercase">
                                                {{ $branches[$i]->name }}
                                            </div>
                                            <div class="col-md-7" style="font-size: 9px;">
                                                {{ $branches[$i]->address }}<br>{{ $branches[$i]->city }}, {{ $branches[$i]->province }} {{ $branches[$i]->postal_code }}
                                            </div>
                                        </div>
                                    </a>
                                </td>
                                @php $bcode = $branches[$i]->id  @endphp
                                @else
                                <td style="width:45%">
                                    <a href="{{ url('branch').'?mid_pos='.$mpost }}" class="btn btn-danger bt1" >Choose kitchen branch</a>
                                </td>
                                @endif
                                <td>
                                    <a href="{{url('process-delete')}}" name="index" value="{{ $i }}" class="fa fa-trash" aria-hidden="true" style="width: 65px"></a>

                                </td>
                            </tr>
                            @endfor
                            @else
                            <tr>
                                <td style="width:45%;">
                                    <a href="{{ url('outlet').'?mid_pos='.$mpost }}" class="btn btn-danger bt1" >Choose {{session('name')}} Outlet</a>
                                </td>
                                <td style="vertical-align:middle;font-size: 10px;font-weight: bold;color: #000000;">Share Data to</td>
                                <td style="width:45%">
                                    <a href="{{ url('branch').'?mid_pos='.$mpost }}" class="btn btn-danger bt1" >Choose kitchen branch</a>
                                </td>
                            </tr>
                            @endif
                        </tbody>
                        <tfoot>
                            <tr>
                                <td></td>
                                <td class="tcntr">
                                    <a href="{{url('daftar_outlet')}}"><img src="{{asset('/images/group-7@3x.png')}}" style="width: 40px;" ></a>
                                </td>
                                <td></td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <div class="row justify-content-md-center" id="ferif" style="display:flex">
                    @php
                    $button = "";
                    if(empty($branches) || empty($outlet))
                    {
                    $button = 'disabled';
                    }
                    elseif(count($branches) != count($outlet))
                    {
                    $button = 'disabled';
                    }

                    @endphp
                    <div class="col-md-2">
                        <button class="btn btn-danger ss11 btnchs" onclick="ferif(this)" {{ $button }}>Next</button>
                    </div>
                </div>
                <form action="{{ url('outlet/store') }}" method="post">
                    <div class="row justify-content-md-center"  id="ferif1" style="display:none">
                        {{ csrf_field() }}
			<input type="hidden" name="mid_pos" value="{{ $mpost }}">
                        <div class="col-md-2">
                            <button class="btn btn-danger ss11 btnchs" onclick="back(this)">Cancel</button>
                        </div>
                        <div class="col-md-4" style="margin:45px 0">
                            <table>
                                <tr>
                                    <td style="padding-right: 10px"><input type="checkbox" id="agree" checked="false" checked="" required></td>
                                    <td style="font-size: 14px;font-weight: bold;color: #000000;">By clicking accept, you agree to share Merchant Profile Data and Data Transaction to us</td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-md-2">
                            <button type="submit" class="btn btn-secondary ss11" id="acc" disabled>Accept</button>
                            {{-- <button class="btn btn-secondary ss11" id="acc" data-toggle="modal" data-target="#finalModal" disabled="true">Accept</button> --}}
                        </div>
                    </div>
                </form>

            </div>
        </div>
        {{-- <div class="modal fade" id="finalModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="background: #fffaf5">
            <div class="modal-dialog" role="document" style="max-width: 1000px">
                <div class="modal-content" style="background: #ff3333;border-radius: 50px;width: 100%;height: 567px;border: none">
                    <div class="modal-header" style="border-bottom: none">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <!--<span aria-hidden="true">&times;</span>-->
                        </button>
                    </div>
                    <div class="modal-body" >
                        <div class="row justify-content-md-center">
                            <b style="font-size: 30px;color: white">KK</b>
                        </div>
                        <div class="row justify-content-md-center" style="margin-top: 81px">
                            <i class="fa fa-check-circle" style="font-size:148px;color: #fffaf5;"></i>
                        </div>
                        <div class="row justify-content-md-center" style="margin-top: 22px">
                            <b style="font-size: 48px;color: #fffaf5;">Thank You!</b>
                        </div>
                        <div class="row justify-content-md-center">
                            <b style="font-size: 16px;color: #ffffff;text-align: center;">Onboarding Merchant is being processed.</b>
                        </div>
                        <div class="row justify-content-md-center" style="margin-top: 23px">
                            <b style="font-size: 14px;color: #ffffff;text-align: center;">You will get username & password to access <br>Merchant Dashboard which will be sent by email.</b>
                        </div>
                    </div>
                    <div class="modal-footer" style="border-top: none">
                    </div>
                </div>
            </div>
        </div> --}}
    </body>
    <script>
        $(document).ready(function () {
            $('#agree').prop('checked', false);
        })

        function ferif() {
            var x = document.getElementById("ferif1");
            var y = document.getElementById("ferif");
            var a = document.getElementById("ferif2");
            var b = document.getElementById("ferif3");
            y.style.display = "none";
            x.style.display = "flex";
            a.style.display = "none";
            b.style.display = "flex";
        }
        function konf1() {
            var t = document.getElementById("acc");
            t.disabled = false;
            t.style.background = "#ff3333";
            t.style.borderColor = "#ff3333";
        }
        function back() {
            var y = document.getElementById("ferif");
            var x = document.getElementById("ferif1");
            y.style.display = "flex";
            x.style.display = "none";
        }


        $('#agree').on('click', function () {
            var elemen = $('#acc');
            if ($(this).is(":checked")) {
                elemen.prop('disabled', false);
                elemen.css('background-color', '#ff3333');
                elemen.css('border-color', '#ff3333');
            } else {
                elemen.prop('disabled', true);
                elemen.css('background-color', '#6c757d');
                elemen.css('border-color', '#6c757d');
            }
        })
    </script>
</html>


