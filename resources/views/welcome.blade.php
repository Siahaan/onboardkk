<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>KitaKitchen</title>

        <!-- Fonts -->
        <link href='https://fonts.googleapis.com/css?family=Rubik' rel='stylesheet'>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <link rel="stylesheet" href="{{ asset('/css/newcss.css') }}">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    </head>
    <body class="fontfamsi">
        <form action="{{ url('konfirmasi') }}" >
            <input type="hidden" name="mid_pos" value="{{ $result->mid_pos }}">
            <div class="container-xl">
                <div class="content">
                    <div class="row justify-content-md-center" >
                        <div class="ss1 col-md-3">
                            <img class="ss4" src="{{asset('/images/kita-kitchen-logo-standard-red@3x.png')}}">
                        </div>
                    </div>
                    <div class="row justify-content-md-center" style="margin-bottom:3.5%">
                        <div class="col-md-4">
                            <img class="ss5" src="{{asset('/images/1-1@3x.png') }}">
                        </div>
                    </div>
                    <div class="row justify-content-md-center ss3">
                        <div class="col-md-10">
                            In this page you will choose {{$result->name}} outlet location<br> you wish to share Merchant Profile and Data Transaction to Kita Kitchen.
                        </div>
                    </div>
                    <div class="row justify-content-md-center" style="text-align: center;">
                        <div class="col-md-2">
                            <button class="btn btn-danger ss2"  type="submit">Next</button>
                        </div>
                    </div>


                </div>
            </div>
        </form>
    </body>
</html>
