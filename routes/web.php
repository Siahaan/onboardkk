<?php
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Route::get('/', 'Controller@index');
Route::post('outlet_pairing', 'Controller@goIntroduction');

Route::get('daftar_outlet', 'Controller@toOutlet');
Route::post('outlet_pairing1', 'Controller@goOutlet');

Route::get('daftar_branch', 'Controller@toBranch');
Route::get('konfirmasi', 'Controller@toKonformasi');

Route::get('konfirmasi_approve', 'Controller@toOutlet');
Route::get('konfirmasi_approve', 'Controller@toKonformasi1');

Route::get('process-approve', 'Controller@doApprove');

Route::get('/callback', 'Controller@index');
Route::get('daftar_outlet', 'Controller@toOutlet');
Route::get('daftar_branch', 'Controller@toBranch');
Route::get('konfirmasi', 'Controller@toKonformasi');
Route::get('outlet', 'Controller@toOutlet');
Route::post('outlet', 'Controller@doOutlet');
Route::get('branch', 'Controller@toBranch');
Route::post('branch', 'Controller@doBranch');
Route::get('konfirmasi_approve', 'Controller@toOutlet');
Route::get('konfirmasi_approve', 'Controller@toKonformasi1');

Route::post('outlet/store','Controller@doApprove');
Route::get('outlet/done','Controller@toDone');
Route::get('process-delete','Controller@doDelete');
// Route::post('addOutletPosts','Controller@doApprove');

